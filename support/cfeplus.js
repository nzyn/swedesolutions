"use strict";

var cfeplus = new function () {
	//////////////////////////////////////////////////////////////////////
	// Private variables
	//////////////////////////////////////////////////////////////////////
	var Hosts = [{ net: "172.16.172", from: 1, to: 1 }, // CFE Plus WiFi AP static address
	//						{ net: "192.168.1",   from: 2, to: 254 }, // Android 8 HTC Hotspot DHCP range
	{ net: "192.168.43", from: 2, to: 5 }, // Android Hotspot DHCP/static range
		//						{ net: "192.168.137", from: 2, to: 254 }, // Windows Hotspot DHCP range
	];
	var RetryCount = 10;
	var TimeoutTime = 2500;

	var Inited = false;
	var Debug = false;

	var Cb_OnReport = {};
	var Cb_OnConnectionChange = {};

	var Ws = {};
	var WsList = [];
	var WsTXq = [];
	var Connected = false;
	var Items = [];
	var Lists = [];
	var SeqNr = 0;

	//////////////////////////////////////////////////////////////////////
	// Public Methods & Properties
	//////////////////////////////////////////////////////////////////////
	this.onReport = function (fnOnReport) {
		_Init();

		if ((typeof (fnOnReport) === 'function')) {
			Cb_OnReport = fnOnReport;
		} else {
			Cb_OnReport = {};
		}

	};

	this.onConnectionChange = function (fnOnConnectionChange) {
		if ((typeof (fnOnConnectionChange) === 'function')) {
			Cb_OnConnectionChange = fnOnConnectionChange;
		} else {
			Cb_OnConnectionChange = {};
		}

		_Init();
	};

	this.get = function (ItemToGet, fnOnUpdate) {
		_Init();

		if (typeof (fnOnUpdate) !== 'function') {
			return;
		}

		var item = _ArraySearch(ItemToGet, 'name', Items);
		if (item == -1) {
			// Create new
			item = Items.push({ name: ItemToGet }) - 1;
		} else {
			// Update existing
		}

		Items[item].fnOnUpdate = fnOnUpdate;	// (Re-)initialize entry
		Items[item].seqnr = _GetSeqNr();

		_Register(item);
	};

	this.unget = function (ItemToUnget) {
		_Init();

		var item = _ArraySearch(ItemToUnget, 'name', Items);
		if (item != -1) {
			Items[item].OnUpdate = {};
			_Unregister(item);
		}
	};

	this.getlist = function (ListToGet, fnOnComplete) {
		_Init();

		if (typeof (fnOnComplete) !== 'function') {
			return;
		}

		var list = _ArraySearch(ListToGet, 'name', Lists);
		if (list == -1) {
			// Create new
			list = Lists.push({ name: ListToGet }) - 1;
		}

		Lists[list].Items = [];// (Re-)initialize entry
		Lists[list].fnOnComplete = fnOnComplete;
		Lists[list].seqnr = _GetSeqNr();

		WsTXq.push('{"cmd":"CFEgetlist","seqnr":' + Lists[list].seqnr + ',"name":"' + Lists[list].name + '","index":0}');

		return (Lists[list].seqnr);
	};

	this.put = function (name, contents) {
		_Init();

		var put = {};
		put.name = (name === undefined) ? '' : name;
		put.contents = {};
		if (contents !== undefined) {
			for (var param of Object.keys(contents)) {
				put.contents[param] = contents[param];
			}
		}

		this.setParameters = function (contents) {
			if (contents !== undefined) {
				for (var param of Object.keys(contents)) {
					put.contents[param] = contents[param];
				}
				return (true);
			}
			return (false);
		};

		this.execute = function () {
			put.cmd = 'CFEput';
			put.seqnr = _GetSeqNr();

			WsTXq.push(JSON.stringify(put));
			return (put.seqnr);
		};
	};

	this.debug = function (onoff) {
		if (onoff) {
			Debug = true;
			_Debug('Debug is now ON');
		} else {
			_Debug('Debug is now OFF');
			Debug = false;
		}
	};

	//////////////////////////////////////////////////////////////////////
	// Private helper functions
	//////////////////////////////////////////////////////////////////////
	function _Init() {
		if (!Inited) {
			Inited = true;

			// Create an array of all potential websocket hosts based on Hosts[]
			WsList.length = 0;
			for (var net = 0; net < Hosts.length; net++) {
				for (var host = Hosts[net].from; host <= Hosts[net].to; host++) {
					var iWs = WsList.push({}) - 1;
					WsList[iWs].url = "ws://" + Hosts[net].net + "." + host + ":81";
					WsList[iWs].ws = {};
					WsList[iWs].timer = {};
				}
			}

			_ReInit();
		}
	}

	function _ReInit() {
		_Connected(false);

		if (Ws.ws instanceof WebSocket) {
			Ws.ws.onopen = function () { };
			Ws.ws.onclose = function () { };
			Ws.ws.onmessage = function () { };
			Ws.ws.onerror = function () { };
			Ws.ws.close();
			Ws.ws = {};
		}

		if (Ws.retryCount && (Ws.retryCount > 0)) {
			var iWs = _ArraySearch(Ws.url, 'url', WsList);
			if (iWs == -1) {
				_Debug(event);
			} else {
				WsList[iWs].ws = new WebSocket(WsList[iWs].url);
				WsList[iWs].ws.onopen = function (evt) { _WsOnOpen(evt); };
				WsList[iWs].ws.onclose = function (evt) { _WsOnClose(evt); };
				WsList[iWs].ws.onmessage = function (evt) { _WsOnMessage(evt); };
				WsList[iWs].ws.onerror = function (evt) { _WsOnError(evt); };
				return; // Sneaky return
			}
		}

		for (var iWs = 0; iWs < WsList.length; iWs++) {
			if (WsList[iWs].retryTimer) {
				clearTimeout(WsList[iWs].retryTimer);
			}
			if (WsList[iWs].ws instanceof WebSocket) {
				WsList[iWs].ws.onopen = function () { };
				WsList[iWs].ws.onclose = function () { };
				WsList[iWs].ws.onmessage = function () { };
				WsList[iWs].ws.onerror = function () { };
				WsList[iWs].ws.close();
				WsList[iWs].ws = {};
			}
			WsList[iWs].ws = new WebSocket(WsList[iWs].url);
			WsList[iWs].ws.onopen = function (evt) { _WsOnOpen(evt); };
			WsList[iWs].ws.onclose = function (evt) { _WsOnClose(evt); };
			WsList[iWs].ws.onmessage = function (evt) { _WsOnMessage(evt); };
			WsList[iWs].ws.onerror = function (evt) { _WsOnError(evt); };
		}
	}

	function _WsOnOpen(event) {
		var iOpenWs = _ArraySearch(event.currentTarget, 'ws', WsList);
		if (iOpenWs == -1) {
			_Debug(event);
		} else {
			Ws = WsList[iOpenWs];
			Ws.ws.send('{"cmd":"CFEnone"}');
			_RegisterAll();
			_Connected(true);

			_Debug('Websocket to ' + Ws.url + ' opened');
		}
	}

	function _WsOnClose(event) {
		if (Ws.ws == event.currentTarget) {
			_DoClose();
		}
	}

	function _WsOnError(event) {
		if (Ws.ws instanceof WebSocket) {
			// We have an open socket
			if (Ws.ws == event.currentTarget) {
				// Error on the socket in use
				_DoClose();
			}
		} else {
			// Re-issue as long as we have no valid open socket
			var iWs = _ArraySearch(event.currentTarget, 'ws', WsList);
			if (iWs != -1) {
				WsList[iWs].retryTimer = setTimeout(function () {
					WsList[iWs].ws = {};
					WsList[iWs].ws = new WebSocket(WsList[iWs].url);
					WsList[iWs].ws.onopen = function (evt) { _WsOnOpen(evt); };
					WsList[iWs].ws.onclose = function (evt) { _WsOnClose(evt); };
					WsList[iWs].ws.onmessage = function (evt) { _WsOnMessage(evt); };
					WsList[iWs].ws.onerror = function (evt) { _WsOnError(evt); };
				}, 250);	// Wait a little before retry
			}
		}
	}

	function _WsOnMessage(event) {
		if (typeof event.data === "string") {
			try {
				var rcv = JSON.parse(event.data);
			} catch (e) {
				_Debug(e);
			}
			if (rcv.hasOwnProperty('cmd')) {
				if (Ws.timerTimeout) {
					clearTimeout(Ws.timerTimeout);
				}
				Ws.timerTimeout = setTimeout(function () { _Debug('WebSocket timeout'); _DoClose(); }, TimeoutTime);	// connection lost timer
				Ws.retryCount = RetryCount;

				switch (rcv.cmd) {
					case 'CFEdata':
						_Debug(event.data);
						for (var i = 0; i < rcv.data.length; i++) {
							var data = rcv.data[i];
							if ((data.name !== undefined) && (data.contents !== undefined)) {
								var item = _ArraySearch(data.name, 'name', Items);
								if (item != -1) {
									if (typeof (data.contents) === 'object') {
										if (typeof (Items[item].contents) === 'object') {
											Items[item].contents = Object.assign(Items[item].contents, data.contents);
										} else {
											Items[item].contents = Object.assign({}, data.contents);
										}
									} else {
										Items[item].contents = data.contents;
									}
									Items[item].fnOnUpdate(Items[item].name, Items[item].contents);
								}
								data.cmd = rcv.cmd;
								data.seqnr = Items[item].seqnr;
								_Debug(data);
							}
						}
						break;

					case 'CFElist':
						_Debug(event.data);
						var list = _ArraySearch(rcv.name, 'name', Lists);
						if (list != -1) {
							if (rcv.hasOwnProperty('eol')) {
								// End Of List, Do callback with the completed list array of objects
								Lists[list].fnOnComplete(Lists[list].name, Lists[list].Items);
							} else {
								if (rcv.data) {
									if ((rcv.hasOwnProperty('index')) && (rcv.index >= Lists[list].Items.length)) {
										Lists[list].Items.push(rcv.data);
										WsTXq.push('{"cmd":"CFEgetlist","seqnr":' + Lists[list].seqnr + ',"name":"' + Lists[list].name + '","index":' + Lists[list].Items.length + '}');
									} else {
										// Duplicate index
										_Debug('Duplicate list index');
										_DoClose();
									}
								}
							}
						}
						break;

					case 'CFEresult':
						_Debug(event.data);
						_Report(rcv.seqnr, rcv.result, rcv.text);
						break;

					case 'CFEnone':
						_Debug(event.data);
						break;

					case 'CFEignore':
						return;	// totally ignore

					default:
						break;
				}
			} else {
				_Debug('Illegal Frame received');
			}

			var cmd;
			if (WsTXq.length > 0) {
				cmd = WsTXq.shift();
				_Debug(cmd);
			} else {
				cmd = '{"cmd":"CFEnone"}';
				//			_Debug(cmd);
			}
			Ws.ws.send(cmd);
		}
	}

	function _Register(item) {
		var Item = Items[item];
		var cmd = '{"cmd":"CFEget","seqnr":' + Item.seqnr + ',"name":"' + Item.name + '"}';
		WsTXq.push(cmd);		// Subscribe
		if (Item.Timer) {
			clearInterval(Item.Timer);
		}
		Item.Timer = setInterval(function () { WsTXq.push(cmd); }, 300000);	// Schedule renewal

		_Debug("Registering " + Item.name);
	}

	function _RegisterAll() {
		for (var item in Items) {
			_Register(item);
		}
	}

	function _Unregister(item) {
		var Item = Items[item];
		var cmd = '{"cmd":"CFEunget","seqnr":' + Item.seqnr + ',"name":"' + Item.name + '"}';
		WsTXq.push(cmd);	// Unsubscribe
		if (Item.Timer) {
			clearInterval(Item.Timer);
			Item.Timer = 0;
		}
		_Debug("Unregistering " + Item.name);
	}

	function _UnregisterAll() {
		for (var item in Items) {
			_Unregister(item);
		}
	}

	function _DoClose() {
		// Socket disconnected, process disconnect
		if (Ws.ws instanceof WebSocket) {
			Ws.ws.onclose = function () { };
			Ws.ws.close();
			_Debug('Websocket to ' + Ws.url + ' closed');
			Ws.ws = {};
		}
		if (Ws.retryCount > 0) {
			Ws.retryCount--;
		}

		while (WsTXq.length > 0) {
			WsTXq.shift();		// clear queue
		}
		_Connected(false);
		setTimeout(_ReInit, 100);	// Try again
	}

	function _Report(SeqNr, MsgNr, MsgText) {
		if ((typeof (Cb_OnReport) === 'function')) {
			Cb_OnReport(SeqNr, MsgNr, MsgText);
		}
	}

	function _Connected(state) {
		// Do user callback
		if ((typeof (Cb_OnConnectionChange) === 'function')) {
			if ((typeof (_Connected.CurrentState) === undefined) || (state != _Connected.CurrentState)) {
				// First time OR state has changed
				_Connected.CurrentState = state;
				Cb_OnConnectionChange(_Connected.CurrentState);
			}
		}
	}

	function _ArraySearch(ValueToSearch, PropertyToSearch, ArrayToSearch) {
		if (ValueToSearch && ArrayToSearch.length) {
			for (var item = 0; item < ArrayToSearch.length; item++) {
				if (ArrayToSearch[item][PropertyToSearch] == ValueToSearch) {
					return (item);
				}
			}
		}
		return (-1);
	}

	function _GetSeqNr() {
		if (++SeqNr >= (1 << 16)) {
			SeqNr = 1;	// use max 16 bits, do not use zero
		}
		return (SeqNr);
	}

	function _Debug(item) {
		if (Debug && window.console) {
			if ((typeof (item) === 'object')) {
				if (item.seqnr) {
					console.log('CFEplus - ' + _PadSpacesLeft(5, item.seqnr) + ': ' + JSON.stringify(item));
				} else {
					var output = '';
					for (var property in item) {
						output += property + ': ' + item[property] + '; ';
					}
					console.log('CFEplus - ' + _PadSpacesLeft(5, 0) + ': ' + output);
				}
			} else {
				try {
					var obj = JSON.parse(item);
					if (obj.seqnr) {
						console.log('CFEplus - ' + _PadSpacesLeft(5, obj.seqnr) + ': ' + JSON.stringify(item));
					} else {
						console.log('CFEplus - ' + _PadSpacesLeft(5, 0) + ': ' + JSON.stringify(item));
					}
				} catch (e) {
					console.log('CFEplus - ' + _PadSpacesLeft(5, 0) + ': ' + item);
				}
			}
		}
	}

	function _PadSpacesLeft(num, input) {
		var pad = " ".repeat(num);
		return (pad + input).slice(-pad.length);
	}
};
